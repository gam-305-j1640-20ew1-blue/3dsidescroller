using UnrealBuildTool;

public class Sidescroller3DTarget : TargetRules
{
	public Sidescroller3DTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Sidescroller3D");
	}
}
